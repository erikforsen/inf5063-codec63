# Acknowledgement: Functionality for creating make rules of dependencies is
# based on code presented here <http://codereview.stackexchange.com/q/11109>

CC = nvcc
CFLAGS = -O0 -g
CUFLAGS = -G -rdc=true \
		  -gencode arch=compute_53,code=sm_53\
		  -gencode arch=compute_20,code=sm_20
LDFLAGS = -lcudart
# Use the compiler to generate make rules. See gcc manual for details.
#MFLAGS = "-MMD -MP -MF"

SOURCES = $(wildcard *.c)
OBJECTS = $(SOURCES:.c=.o)
CUDASOURCES = $(wildcard *.cu)
CUDAOBJECTS = $(CUDASOURCES:.cu=.o)
#DEPENDENCIES = $(addprefix .,$(SOURCES:.c=.d))  # Add dot prefix to hide files.

.PHONY: clean  all

all: c63enc_gpu c63dec c63pred

c63dec: c63dec.c dsp.o tables.o io.o common.o me.o
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $@
c63pred: c63dec.c dsp.o tables.o io.o common.o me.o
	$(CC) $^ -DC63_PRED $(CFLAGS) $(LDFLAGS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%.o: %.cu
	$(CC) $(CFLAGS) $(CUFLAGS) -c $<

c63enc_gpu: c63enc.o dsp.o tables.o io.o c63_write.o common.o me.o
	$(CC) $(CFLAGS) -o c63enc_gpu $^ $(LDFLAGS)

clean:
	$(RM) c63enc c63dec c63pred c63enc_gpu $(OBJECTS) $(CUDAOBJECTS) $(DEPENDENCIES) test.c63 test.yuv

foreman: 
	./c63enc_gpu -w 352 -h 288 -f 10 -o test.c63 /mnt/sdcard/foreman.yuv
	./c63dec test.c63 test.yuv
#-include $(DEPENDENCIES)
